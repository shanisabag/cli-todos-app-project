import { todoList } from "../types/types.js";
export declare function showTasks(tasks: todoList): void;
export declare function showHelpWindow(): void;
