import { todoList } from "../types/types.js";
export declare function execute(path: string, args: string[], todos: todoList): Promise<void>;
