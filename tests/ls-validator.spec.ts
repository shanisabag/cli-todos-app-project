import { expect } from "chai";
import { lsValidator } from "../src/controller/cli-validator";

describe("The lsValidator module", () => {
    context("lsValidator", () => {
        it("should exist", () => {
            expect(lsValidator).to.be.instanceOf(Function);
        });

        it("should return true if no arguments after ls", () => {
            const actual = lsValidator([]);
            expect(actual).to.equal(true);
        });

        it("should return true for ls -c command", () => {
            const actual = lsValidator(["-c"]);
            expect(actual).to.equal(true);
        });

        it("should return true for ls -a command", () => {
            const actual = lsValidator(["-a"]);
            expect(actual).to.equal(true);
        });

        it("should return false if there is more than one argument after ls", () => {
            const actual = lsValidator(["-c", "-a"]);
            expect(actual).to.equal(false);
        });
    });
});