import { expect } from "chai";
import { editValidator } from "../src/controller/cli-validator";

describe("The editValidator module", () => {
    context("editValidator", () => {
        const todos = [
            {
                id: "gbo02b1dne8",
                complete: false,
                description: "hw 1"
            },
            {
                id: "gbo02b1dne5",
                complete: false,
                description: "hw 2"
            }
        ];

        it("should exist", () => {
            expect(editValidator).to.be.instanceOf(Function);
        });

        it("should return true for a valid id with description", () => {
            const actual = editValidator(["gbo02b1dne8", "new description"], todos);
            expect(actual).to.equal(true);
        });

        it("should return false for an invalid id", () => {
            const actual = editValidator(["gbo02b1dne9", "new description"], todos);
            expect(actual).to.equal(false);
        });
    });
});