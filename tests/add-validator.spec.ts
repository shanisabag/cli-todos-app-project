import { expect } from "chai";
import { addValidator } from "../src/controller/cli-validator";

describe("The addValidator module", () => {
    context("addValidator", () => {
        it("should exist", () => {
            expect(addValidator).to.be.instanceOf(Function);
        });

        it("should return true", () => {
            const actual = addValidator(["hw 1"]);
            expect(actual).to.equal(true);
        });

        it("should return false if there is more than one task", () => {
            const actual = addValidator(["hw 1", "hw 2"]);
            expect(actual).to.equal(false);
        });
    });
});