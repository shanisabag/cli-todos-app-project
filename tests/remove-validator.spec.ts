import { expect } from "chai";
import { removeValidator } from "../src/controller/cli-validator";

describe("The removeValidator module", () => {
    context("removeValidator", () => {
        const todos = [
            {
                id: "gbo02b1dne8",
                complete: false,
                description: "hw 1"
            },
            {
                id: "gbo02b1dne5",
                complete: false,
                description: "hw 2"
            }
        ];

        it("should exist", () => {
            expect(removeValidator).to.be.instanceOf(Function);
        });

        it("should return true for a valid id", () => {
            const actual = removeValidator(["gbo02b1dne8"], todos);
            expect(actual).to.equal(true);
        });

        it("should return false for an invalid id", () => {
            const actual = removeValidator(["gbo02b1dne9"], todos);
            expect(actual).to.equal(false);
        });

        it("should return true for rm -c command", () => {
            const actual = removeValidator(["-c"], todos);
            expect(actual).to.equal(true);
        });

        it("should return false if there are arguments after rm -c command", () => {
            const actual = removeValidator(["-c", "f"], todos);
            expect(actual).to.equal(false);
        });
    });
});