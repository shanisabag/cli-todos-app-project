export interface ITodo {
    id: string;
    complete: boolean;
    description: string;
}

export type todoList = ITodo[];
